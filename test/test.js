const fs = require('fs');
const expect = require('chai').expect;
const validator = require('../index');

describe('validate svg', () => {
    it('should report success for valid svg sample 1', (done) => {
        fs.readFile('test/samples/good.svg', (err, data) => {
            if (err) done(err);

            validator.check(data, { type: 'svg' })
                .then(result => {
                    expect(result.success).to.be.true;
                    expect(result.messages.length).to.equal(0);
                    done();
                })
                .catch(done);
        });
    });

    it('should report success for valid svg sample 2', (done) => {
        fs.readFile('test/samples/new.svg', (err, data) => {
            if (err) done(err);

            validator.check(data, { type: 'svg' })
                .then(result => {
                    expect(result.success).to.be.true;
                    expect(result.messages.length).to.equal(0);
                    done();
                })
                .catch(done);
        });
    });

    it('should report errors on invalid svg sample 3', (done) => {
        fs.readFile('test/samples/bad.svg', (err, data) => {
            if (err) done(err);

            validator.check(data, { type: 'svg' })
                .then(result => {
                    expect(result.success).to.be.false;
                    expect(result.messages.length).to.equal(1);
                    done();
                })
                .catch(done);
        });
    });


    it('should report errors on invalid svg sample 4', (done) => {
        fs.readFile('test/samples/original.svg', (err, data) => {
            if (err) done(err);

            validator.check(data, { type: 'svg' })
                .then(result => {
                    expect(result.success).to.be.false;
                    expect(result.messages.length).to.equal(1);
                    done();
                })
                .catch(done);
        });
    });
});

describe('validate html', () => {
    it('should report success for valid html file', (done) => {
        fs.readFile('test/samples/good.html', (err, data) => {
            if (err) done(err);

            validator.check(data)
                .then(result => {
                    expect(result.success).to.be.true;
                    expect(result.messages.length).to.equal(0);
                    done();
                })
                .catch(done);
        });
    });

    it('should report errors on invalid html file', (done) => {
        fs.readFile('test/samples/bad.html', (err, data) => {
            if (err) done(err);

            validator.check(data)
                .then(result => {
                    expect(result.success).to.be.false;
                    expect(result.messages.length).to.equal(41);
                    done();
                })
                .catch(done);
        });
    });
});
