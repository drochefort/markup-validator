const request = require('request');
const presets = require('./presets');

const validTypes = Object.keys(presets);
const delay = 250; // Avoid flooding the w3c api
const uri = 'https://validator.nu';
const method = 'POST';
const out = 'json';

function check(content, options = { type: 'html' }) {
    const type = options.type || 'html';
    if (validTypes.includes(type)) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                const formData = {
                    out,
                    schema: presets[type].schema,
                    parser: presets[type].parser,
                    content,
                };
                
                let data = '';

                request({ method, uri, formData })
                    .on('data', chunk => data += chunk.toString())
                    .on('end', () => {
                        const result = JSON.parse(data).messages;
                        if (result && result.map) {
                            const success = result && result.length === 0;
                            const messages = result.map(m => `Lines ${m.firstLine}-${m.lastLine}: ${m.message}`);
                            resolve({ success, messages });
                        } else {
                            reject(new Error('corrupted output', data));
                        }
                    })
                    .on('error', reject);
            }, delay);
        });
    } else {
        throw new Error(`input content type ${type} must be part of this list: ${validTypes.join(',')}`);
    }
}

module.exports = { check };
