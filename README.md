## Synopsis

Validate HTML/XML/SVG markup against the standards, using the W3C Markup Validation Service.
It uses the latest validation service: https://about.validator.nu/


## Code Example

```
const fs = require('fs');
const validator = require('@hmh/markup-validator');

fs.readFile('test/samples/good.html', (err, data) => {
    if (err) {
    	console.error(err);
    }

    validator.check(data, { type: 'html' })
        .then(result => {
            console.log('RESULT', result.success);
        })
        .catch(e => {
        	console.error(e);
        });
});

```

## Installation

```
npm install --save @hmh/markup-validator
```

## API Reference

Depending on the size of the project, if it is small and simple enough the reference docs can be added to the README. For medium size to larger projects it is important to at least provide a link to where the API reference docs live.

### check(data:string, options?:any)

Call this method to validate some markup.
This method takes 2 parameters:

1. *data* (mandatory) the data to validate
2. *options* (optional) see below for available options

### options

1. *type*: a string with possible value: `html`, `xml`, `svg` (default: `html`). It sets the type of content to validate.

## Tests

Tests are written with Mocha, and source code is located under the `test/` folder.

```
npm install -g mocha
npm test
```

## Contributors

@drochefort

## License

This is a private HMH package